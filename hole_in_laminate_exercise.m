% The ply is specially orthotropic (for now)
% so the 1-2 axis coincides with the ref axis
% We use the same ply properties as before
close all
clear
clc

ply_properties;
a = 0:1:90;

% The ply is subjected to axial stress only - N/mm^2
f1 = 100;
f2 = 0;
f12 = 0;
F_init = [f1;f2;f12];
% Things we will need down the road
k = sqrt(E1/E2);
nn = sqrt((2*(k-v12))+(E1/G12));
fa = zeros(size(a));
Ea = zeros(size(a));
% Fa = zeros(3,size(a,2));
fx = zeros(size(a));
fy = zeros(size(a));
fxy = zeros(size(a));
% Tsai - Hill criterion
F_est = -1/2;
F1 = (1/Xt) - (1/Xc);
F2 = (1/Yt) - (1/Yc);
F11 = 1/(Xt*Xc);
F22 = 1/(Yt*Yc);
F33 = 1/(Sh^2); % Remember the max shear is Sh not S
F12 = (F_est)*(sqrt(F11*F22));
fx_TH = zeros(size(a));
% Max Stress Theory to get the mode or the probable mode
FI1a = zeros(size(a));
FI2a = zeros(size(a));
FI12a = zeros(size(a));
% F_mat = zeros(size(a));

for i = 1:size(a,2)
    alfred_iter;
    % Young's modulus at the angular position a
    p = ((sind(a(i))^4)/E1) + (((1/G12) - ((2*v12)/E1))*(sind(a(i))^2) ...
    *(cosd(a(i))^2)) + ((cosd(a(i))^4)/E2);
    Ea(i) = 1/p;
    
    A = ((-k*(cosd(a(i))^2)) + (1+nn)*(sind(a(i))^2));
    B = k*(((k+nn)*(cosd(a(i))^2)) - (sind(a(i))^2));
    C = (1+k+n)*(nn)*sind(2*a(i));

    fa(i) = (Ea(i)/E1)*(A*f1 +B*f2 + C*f12);
    
    fx(i) = fa(i)*(sind(a(i))^2);
    fy(i) = fa(i)*(cosd(a(i))^2);
    fxy(i) = -fa(i)*((sind(a(i))*cosd(a(i))));
    F_mat = [fx(i);fy(i);fxy(i)];

    % Tsai - Hill
    if F_mat(1) > 0
        X1 = Xt;
    else X1 = Xc;
    end
    
    if F_mat(2) > 0
        Y1 = Yt;
    else Y1 = Yc;
    end
    
    if F_mat(3) > 0
        X2 = Xt;
    else X2 = Xc;
    end
    
    FI_Hill(i) = (F_mat(1)/X1)^2 + (F_mat(2)/Y1)^2 + (F_mat(3)/Sh)^2 ...
    - ((F_mat(1)/X1)*(F_mat(2)/X2));

    % Max Stress Theory for the mode
    FI1a(i) = F_mat(1)/X1;
    FI2a(i) = abs(F_mat(2)/Y1);
    FI12a(i) = F_mat(3)/Sh;
end

plot(a,fa,a,zeros(size(a)))

M = [a' Ea' fa' fx' fy' fxy' FI_Hill' ];