% close all
% clear
% clc

% First run the stiffness analysis
% laminate_stiffness_analysis;

% Force intensities - N/mm
% Nx = 100;
% Ny = 0;
% Nxy =0;
% N = [Nx;Ny;Nxy];

% % Moment intensities - Nmm/mm
% Mx = 0;
% My = 0;
% Mxy = 0;
% M = [Mx;My;Mxy];

% % Force/Moment vector
% NM = [N;M];

% Uncomment all of the above if you want to run this on its own
% Stiffness relationships
full_stiffness_matrix = [A B; B D];
% Compliance - Better put it together manually
% than have it invert the whole matrix
% full_compliance_matrix = [a b; b d]; This is wrong but lets keep
% here for future reference
full_compliance_matrix = inv(full_stiffness_matrix);

e_0_and_k = full_compliance_matrix*NM; % 6x1
% At this point matlab's inv function breaks down and
% gives me Inf on the b matrix. We will populate it with
% some very very small values right before the inversion
% so that it does not go crazy
% edit: if I just invert it like I do above this does not
% need to be done

e_0 = e_0_and_k(1:3);
k_ref = e_0_and_k(4:6);

% membrane_load = 0;
% bending_load = 0;
% if any(N)
%     membrane_load = 1;
% end

% if any(M)
%     bending_load = 1;
% end

% Membrane Strains - This is a constant strain
% distribution through the laminate thickness
% in all the plies
% e_0 = a*N;

% % Curvatures
% k_ref = d*M;

% Total strains for each ply
e_ref = zeros(3,1,n_of_plies);

for i = 1:n_of_plies
    e_ref(:,:,i) = e_0 - z_mp(i)*k_ref;
end
% Angle composition of the plies bottom to top
% theta = [0; 90];
% We commment it out to supply it at another script, if used alone, uncomment
% For each ply get the strains at the material axis
e_mat = zeros(3,1,n_of_plies);

for i = 1:n_of_plies
    alfred_iter;
    e_mat(:,:,i) = mn_strain_ref_to_mat*e_ref(:,:,i);
end

% For each ply get the strains at the material axis
f_mat = zeros(3,1,n_of_plies);

for i = 1:n_of_plies
    alfred_iter;
    f_mat(:,:,i) = Q(:,:,i)*e_mat(:,:,i);
end

% Failure indexes for each ply
FI1 = zeros(n_of_plies,1);
FI2 = zeros(n_of_plies,1);
FI12 = zeros(n_of_plies,1);
FI_Hill = zeros(n_of_plies,1);

for i = 1:n_of_plies
    % Check to see whether we should use compression or tension variables
    if f_mat(1,:,i) >= 0
        X1 = Xt;
    elseif f_mat(1,:,i) < 0
        X1 = Xc;
    end

    if f_mat(2,:,i) >= 0
        Y1 = Yt;
        X2 = Xt;
    elseif f_mat(2,:,i) < 0
        Y1 = Yc;
        X2 = Xc;
    end
    % Tsai - Hill
    FI_Hill(i) = (f_mat(1,:,i)/X1)^2 + (f_mat(2,:,i)/Y1)^2 + ...
    (f_mat(3,:,i)/Sh)^2 - ((f_mat(1,:,i)/X1)*(f_mat(2,:,i)/X2));
    
    % Max Stress Theory for the mode
    FI1(i) = abs(f_mat(1,:,i)/X1);
    FI2(i) = abs(f_mat(2,:,i)/Y1);
    FI12(i) = abs(f_mat(3,:,i)/Sh);
end

% Determine MOF (mode of failure)
% LT = longitudal tension - fibre direction
% TT = transverse tension - matrix direction
% LC = longitudal compression - fibre direction
% TC = transverse compression - matrix direction
% S = in-plane shear
MOF = cell(n_of_plies,1);

for i = 1:n_of_plies
    % If FI1 is the largest and the stress is tensile, write LT
    if FI1(i) > FI2(i) && FI1(i) > FI12(i) && f_mat(1,:,i) > 0
        MOF(i) = {'LT'};
    % else, if the stress is compressive, write LC
    elseif FI1(i) > FI2(i) && FI1(i) > FI12(i) && f_mat(1,:,i) < 0
        MOF(i) = {'LC'};
    % If FI2 is the largest and the stress is tensile write TT
    elseif FI2(i) > FI1(i) && FI2(i) > FI12(i) && f_mat(2,:,i) > 0
        MOF(i) = {'TT'};
    % else, if the stress is compressive, write TC
    elseif FI2(i) > FI1(i) && FI2(i) > FI12(i) && f_mat(2,:,i) < 0
        MOF(i) = {'TC'};
    % If FI12 is the largest, write S
    elseif FI12(i) > FI1(i) && FI12(i) > FI2(i)
        MOF(i) = {'S'};
    end
end

% Results in a table for easier checking
results_f1 = zeros(n_of_plies,1);
results_f2 = zeros(n_of_plies,1);
results_f12 = zeros(n_of_plies,1);
for i = 1:n_of_plies
    results_f1(i) = f_mat(1,:,i);
    results_f2(i) = f_mat(2,:,i);
    results_f12(i) = f_mat(3,:,i);
end


Ply = (1:n_of_plies)';
Angle = theta;
f1 = results_f1;
f2 = results_f2;
f12 = results_f12;

clc % To get rid of all these warnings about the matrices

Results = table(Ply,Angle,f1,f2,f12,FI1,FI2,FI12,MOF)

% f_mat(1,:,:)
% f_mat(2,:,:)
% FI1
% FI2
% FI12
% FI_Hill