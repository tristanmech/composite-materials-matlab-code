% Elastic Constants
E1 = 140; % KN/mm^2
E2 = 10;
G12 = 5;
v12 = 0.3;

theta = 30; % degrees

% Do some calculations
alfred;
mn_matrix

% Reduced matrices
Q = Reduced_Stiffness(E1,E2,G12,v12,v21)
S = inv(Q)

% Transformed matrices
trans_Q = Transformed_Stiffness(Q,mn_matrix)
trans_S = inv(trans_Q)

% Transformed elastic constants
[Ex,Ey,vxy,vyx,mx,my,Gxy] = Transformed_Elastic_Constants(trans_S)

% maybe do some iterations and see how they change as we vary the angle theta