% Example 4.3
close all
clear
clc

% We will determine the strain and stress at 3 points:
% The top, the border between the materials and the bottom

% Properties - N/mm^2
P = 10e3;
E_A = 70e3;
E_B = 210e3;
t_A = 40;
t_B = 10;
b = 10;
% Axial Stiffness
A = E_A*t_A + E_B*t_B;

% Coupling Stiffness
B = (E_A*t_A*t_B)/2 - (E_B*t_A*t_B)/2;

% Bending Stiffness
D = (E_A*(t_A^3 + 3*t_A*(t_B^2)))/12 + (E_B*(t_B^3 + 3*(t_A^2)*t_B))/12;

Nx = P/b;

k_x = (1000*B)/(B^2 - D*A);
e_x0 = (1000 - B*k_x)/A;
z = [((t_A+t_B)/2); (t_A - t_B)/2; -(t_A+t_B)/2]; % Top to bottom

% e_x = zeros(3,1);
% f_xA = zeros(3,1);
% f_xB = zeros(3,1);

% for i = 1:size(z)
%     e_x(i) = e_x0 - z(i)*k_x;
%     f_xA(i) = E_A*e_x(i);
%     f_xB(i) = E_B*e_x(i);
% end

% No need for a loop after all

e_x =  e_x0 - z*k_x
f_xA = E_A * e_x
f_xB = E_B * e_x

% Example 