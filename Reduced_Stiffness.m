function y = Reduced_Stiffness(E1,E2,G12,v12,v21)
% Supply the elastic constants and get the Q matrix
% 
% Syntax: y = Reduced_Stiffness(E1,E2,G12,v12)
%

y = [
    E1/(1-v12*v21) (v21*E1)/(1-v12*v21) 0;
    (v12*E2)/(1-v12*v21) E2/(1-v12*v21) 0;
    0 0 G12
];
    
end