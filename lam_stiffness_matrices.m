function [A,B,D] = lam_stiffness_matrices(trans_Q,tp,z_mp)
% Get the stiffness matrices for a given laminate
% Trans_Q must contain the transformed matrix for each
% ply so that we can sum them all up. 
% THIS IS SUPPOSED TO RUN IN A LOOP
% We'll optimize later (hopefully)
% tp = ply thinkness
% z_mp = ply centroid coordinate relative to the midplane


A = (tp)*trans_Q;

B = (-tp*z_mp')*trans_Q;

D = ((tp*(z_mp^2')) + (tp^3)/12)*trans_Q;

end