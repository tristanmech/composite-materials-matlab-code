% Plot the change of the transformed elastic constants
% as the ply angle changes
close all
clear
clc
% Elastic Constants
E1 = 70; % KN/mm^2
E2 = 70;
G12 = 5;
v12 = 0.1;
v21 = (v12*E2)/E1;

% Ply angles from 0 to 90 degrees
theta = 0:2:90;

Ex = [];
Ey = [];
vxy = [];
vyx = [];
mx = [];
my = [];
Gxy = [];

for i = 1:size(theta,2)
    alfred_iter
    
    % Reduced matrices
    Q = Reduced_Stiffness(E1,E2,G12,v12,v21);
    S = inv(Q);

    % Transformed matrices
    trans_Q = Transformed_Stiffness(Q,mn_matrix);
    trans_S = inv(trans_Q);

    [Ex(i),Ey(i),vxy(i),vyx(i),mx(i),my(i),Gxy(i)] = Transformed_Elastic_Constants(trans_S);
end

title('Ex,Ey,Gxy variation with ply angle')
ylabel('E(kN/mm^2')
xlabel('Ply angle')
plot(theta,Ex,theta,Ey,theta,Gxy)
legend({'Ex','Ey','Gxy'})

figure
title('vxy,vyx variation with ply angle')
ylabel('v')
xlabel('Ply angle')
plot(theta,vxy,theta,vyx)
legend({'vxy','vyx'})

figure
title('mx,my variation with ply angle')
ylabel('m')
xlabel('Ply angle')
plot(theta,mx,theta,my)
legend({'mx','my'})

figure
ylabel('G(kN/mm^2')
xlabel('Ply angle')
plot(theta,Gxy)