%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 3.1 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear
clc
% N/mm^2
fx = 150;
fy = -50;
fxy = 40;

Xt = 200;
% Calculemus!
fI = (fx + fy)/2 + (1/2)*(sqrt(((fx-fy)^2) + 4*fxy^2));
fII = (fx + fy)/2 - (1/2)*(sqrt(((fx-fy)^2) + 4*fxy^2));

% Rankine theory 
% Failure if fI > Xt / fII > Xc
disp('Rankine:')
if fI > Xt
    disp('Failure!!')
else
    disp('No probs')
end

% Tresca
% Failure if (fI - fII)> Xt
disp('Tresca:')
if (fI - fII) > Xt
    disp('Failure!!')
else
    disp('No probs')
end

% Von Mises
% Failure if sqrt(fI^2 + fII^2 - fI*fII) > Xt
disp('Von Mises:')
if sqrt(fI^2 + fII^2 - fI*fII) > Xt
    disp('Failure!!')
else
    disp('No probs')
end

disp('')
disp('')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 3.3 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear
clc
ply_properties
% all the stresses are in the material directions
% hurray for us
f1 = 800;
f2 = -100;
f12 = 40;
F = [f1; f2; f12];
% Failure Indexes - If anything is over or equal to 1 you
% are screwed
% (a)
FI1a = f1/Xt;
FI2a = abs(f2/Yc);
FI12a = f12/Sh;

FIa = [FI1a; FI2a; FI12a]
% (b)
% We need to calculate Q, and S first
Q = Reduced_Stiffness(E1,E2,G12,v12,v21);
S = inv(Q);

e = S*F;
% Now that we have the strains we need to construct
% the failure indexes
FI1b = e(1)/ex_t;
FI2b = abs(e(2)/ey_c);
FI12b = abs(e(3)/es);

FIb = [FI1b; FI2b; FI12b]

% (c) Tsai - Hill
FIc  = (f1/Xt)^2 + (f2/Yc)^2 + (f12/Sh)^2 -((f1/Xt)*(f2/Xc))

% (e) Tsai - Wu
F_est = -1/2; % This needs to be determined from a test

F1 = (1/Xt) - (1/Xc);
F2 = (1/Yt) - (1/Yc);
F11 = 1/(Xt*Xc);
F22 = 1/(Yt*Yc);
F33 = 1/(Sh^2); % Remember the max shear is Sh not S
F12 = (F_est)*(sqrt(F11*F22));

FIe = F1*f1 + F2*f2 + F11*f1^2 + F22*f2^2 + F33*f12^2 + 2*F12*f1*f2

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 3.4 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear
clc
% Ply Properties
% N/mm^2
ply_properties;
theta = 45;
alfred;
% Stress in the reference axis - N/mm^2
fx = -100;
fy = 50;
fxy = 10;
F_ref = [fx; fy; fxy];
% To do strength analysis, first we need to determine
% the equivalent stresses in the material axis
F_mat = mn_stress_ref_to_mat*F_ref

% Tsai - Hill
% The following part gave a wrong result because I put Yt in
% the second term because fy was tensile without checking first
% whether the resulting f2 was tensile as well. Turns out it is not
% so we need to put Yc instead and Xc at the last term
FI = (F_mat(1)/Xc)^2 + (F_mat(2)/Yc)^2 + (F_mat(3)/Sh)^2 ...
 - ((F_mat(1)/Xc)*(F_mat(2)/Xc))
% This will indicate whether failure has occured or not
% but to get the mode of failure we need to apply the
% maximum stress theory criteria
FI1a = F_mat(1)/Xc;
FI2a = abs(F_mat(2)/Yt);
FI12a = F_mat(3)/Sh;

if FI1a > 1
    disp('Longitudal Failure')
elseif FI2a > 1
    disp('Transverse Failure')
elseif FI12a > 1
    disp('Shear Failure')
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 3.5 (not) %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Turns out this is not what the exercise wants but it's interesting
% so we'll keep it. It shows whether the ply will fail under the same
% load with different fibre orientations. To determing whether failure
% has occured Tsai - Hill and Tsai - Wu are used, and to determing the
% mode maximum stress theory
clear
clc
close all

ply_properties;
% Stresses
fx = -100;
fy = 50;
fxy = 10;
F_ref = [fx; fy; fxy];

% Angle
theta = 0:2:90;
X1 = 0;
X2 = 0;
Y1 = 0;
FI_Hill = zeros(size(theta));
% Tsai - Wu calculations
F_est = -1/2;

F1 = (1/Xt) - (1/Xc);
F2 = (1/Yt) - (1/Yc);
F11 = 1/(Xt*Xc);
F22 = 1/(Yt*Yc);
F33 = 1/(Sh^2); % Remember the max shear is Sh not S
F12 = (F_est)*(sqrt(F11*F22));
FI_Wu = zeros(size(theta));

% Max stress theory
FI1a = zeros(size(theta));
FI2a = zeros(size(theta));
FI12a = zeros(size(theta));

for i = 1:size(theta,2)
    alfred_iter;
    F_mat = mn_stress_ref_to_mat*F_ref;
    % Tsai - Hill
    % We add a check to see if the resulting stress in the
    % material axis is tensile or compressive, and the choose
    % the correct ultimate(!!!!!) strength for the criterion terms
    if F_mat(1) > 0
        X1 = Xt;
    else X1 = Xc;
    end
    
    if F_mat(2) > 0
        Y1 = Yt;
    else Y1 = Yc;
    end
    
    if F_mat(2) > 0
        X2 = Xt;
    else X2 = Xc;
    end
    
    FI_Hill(i) = (F_mat(1)/X1)^2 + (F_mat(2)/Y1)^2 + (F_mat(3)/Sh)^2 ...
    - ((F_mat(1)/X1)*(F_mat(2)/X2));

    % Tsai - Wu
    FI_Wu(i) = F1*F_mat(1) + F2*F_mat(2) + F11*F_mat(1)^2 + F22*F_mat(2)^2 ...
    + F33*F_mat(3)^2 + 2*F12*F_mat(1)*F_mat(2);

    % Max Stress Theory for the mode
    FI1a(i) = F_mat(1)/X1;
    FI2a(i) = abs(F_mat(2)/Y1);
    FI12a(i) = F_mat(3)/Sh;

end

plot(theta,FI_Hill,theta,FI_Wu,theta, ones(size(theta))...
,theta, FI1a, theta, FI2a, theta, FI12a)
legend({'Tsai - Hill','Tsai - Wu','Failure Threshold','FI - Fibre Direction'...
, 'FI - Matrix Direction', 'FI - Shear Mode'})
% plot(theta,FI_Wu)
% legend({'Tsai - Wu'})
% plot(theta, ones(size(theta)))

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 3.5 (proper) %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear
clc
close all

ply_properties;

% Stresses - We only have longitudal stress now

% We want to see at what stress will the ply fail
% according to Tsai - Hill/Wu so we set both FIs at 1

% f1 = m^2 *fx = fx * cos^2()
% f2 = n^2 *fx = fx * sin^2()
% f12 = -mn^2 *fx = -fx*cos()sin()
% f1 > 0 for 0 < theta < 90 so X = Xt
% f2 > 0 for 0 < theta < 90 so Y = Yt
% Let's put all that in the equation
theta = 0:2:90;
si = sind(theta);
co = cosd(theta);
fx_TH = zeros(size(theta));
% fx_TW = zeros(size(theta,2));

F_est = -1/2;
F1 = (1/Xt) - (1/Xc);
F2 = (1/Yt) - (1/Yc);
F11 = 1/(Xt*Xc);
F22 = 1/(Yt*Yc);
F33 = 1/(Sh^2); % Remember the max shear is Sh not S
F12 = (F_est)*(sqrt(F11*F22));
a = zeros(size(theta));
b = zeros(size(theta));
syms x
for i = 1:size(theta,2)
    % Tsai - Hill
    p = ((co(i)^2)/Xt)^2 + ((si(i)^2)/Yt)^2 ...
        + ((si(i)*co(i))/Sh)^2 - ((si(i)*co(i))/Xt)^2;
    fx_TH(i) = sqrt(1/p);
    
    % Tsai - Wu
    % a(i) = (co(i)^2)*F1 + (si(i)^2)*F2;
    % b(i) = ((co(i)^4)*F11) + (si(i)^4)*F22 + ...
    %     (-((co(i)^2) * (si(i)^2))*F33) + ...
    %     2*(((co(i)^2) * (si(i)^2))*F12);
    % eqn = (x^2)*b + x*a - 1 == 0;
    % fx_TW(i) = solve(eqn,x);
end

plot(theta,fx_TH)

% eqn = (x^2)*b + x*a - 1 == 0;

% Turns out solving a quadratic equation can be more complicated
% that it seems (thank matlab precision) so I'm going to skip that
% to save some time but the TH one works like a charm