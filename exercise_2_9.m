clear
clc
close all

%    (a) Determine the corresponding strains in the reference (x-y) axes by
%    using the strain-stress relationship of a generally orthotropic ply.
%    Compare these strain values with those obtained by making use of
%    the equivalent engineering elastic constants for a generally orthotropic
%    ply.
%    (b) Obtain the strains in the material axes (1-2), by using the strain values
%    obtained in (a) above by considering the strain-stress relationship of a
%    generally orthotropic ply.
%    (c) Determine the stresses in the material axes (1-2) by using the
%    stress-strain relationship of a specially orthotropic ply.
%    Compare these stress values with those obtained by making use of
%    the stress transformation from the reference (x-y) to the material
%    (1-2) axes.

% Elastic Constants - KN/mm^2
E1 = 140;
E2 = 10;
G12 = 5;
v12 = 0.3;
v21 = (v12*E2)/E1;
theta = 45;
alfred;

% Reduced matrices
Q = Reduced_Stiffness(E1,E2,G12,v12,v21);
S = inv(Q);

% Transformed matrices
trans_Q = Transformed_Stiffness(Q,mn_matrix);
trans_S = inv(trans_Q);

% Stresses in reference x-y axis - kN/mm^2
fx = 0.05;
fy = 0.01;
fxy = -0.01;

% Stresses in matrix form
F = [0.05; 0.01; -0.01];

% (a)
% Lets do matrix multiplication!
% Calculate strains in the x-y reference axis
e_ref = zeros(3,1);
e_ref = trans_S*F

% We could also get the transformed constants
% and calculate the strains from there

% (b)
% Obtain strains in material axis
e_mat = zeros(3,1);
e_mat = mn_strain_ref_to_mat*e_ref

% (c)
% Obtain stresses by using the Q matrix and then
% again by transforming the xy ones to 12
F_mat1 = zeros(3,1);
F_mat2 = zeros(3,1);

F_mat1 = Q*e_mat
F_mat2 = mn_stress_ref_to_mat*F