function [Ex,Ey,vxy,vyx,mx,my,Gxy] = trans_elastic_constants(trans_S)
% Get the transformed constants in the x-y reference axis by supplying the
% transformed compliance matrix
%
% Syntax: [Ex,Ey,vxy,vyx,mx,my] = trans_elastic_constants(trans_S)
% 

Ex = 1/trans_S(1,1);

Ey = 1/trans_S(2,2);

vxy = -(Ex*trans_S(1,2));

vyx = (vxy*Ey)/Ex;

mx = -(Ex*trans_S(1,3));

my = -(Ey*trans_S(2,3));

Gxy = 1/(trans_S(3,3));
    
end