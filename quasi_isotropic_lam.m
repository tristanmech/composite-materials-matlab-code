function y = quasi_isotropic_lam(n_of_set_plies)
% Provide the number of plies you wish to have in the set
% and this will give you all the angles. It will also
% create a new file for each different ply configuration
% and generate the composition vector (at some point it will
% do all these things but not yet)
%
% Syntax: y = myFun(number of plies in the set)
y = zeros(n_of_set_plies,1);

for p = 1:n_of_set_plies
    y(p) = 180*(p-1)/n_of_set_plies;
    if y(p) > 90
        y(p) = y(p) - 180;
    end
end

end