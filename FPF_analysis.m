close all
clear
clc

% Input the composition of the laminate,the angles and the force/moment
% vector (containing all the stuff acting on the laminate) and this will
% do FPF analysis
%
% composition: a vector containing the layout of the laminate e.g
% comp = [0;1;1;0]
% The first element is the bottom ply
% 0 - carbon/epoxy, 0 degrees
% 1 - carbon/epoxy, 45 degrees
% 2 - carbon/epoxy, -45 degrees
% 3 - carbon/epoxy, 90 degrees
% 4 - carbon/epoxy, same degrees as before but with elastic constants at 0
% 5 - same as before but will E2,G12 = 0 and E1 like before
% This whole thing is not so smart but it works for the moment so give me a break
composition = [4;0;0;4];
theta = [90;0;0;90];
laminate_stiffness_analysis;

% theta = [0;45;-45;90;90;-45;45;0];
% This whole theta thing happens because it changes value in the
% stiffness analysis. I know, its stupid. Consquently it needs to be
% after the stiffness analysis
% edit: it doesn't any more!

% NM = [Nx; Ny; Nxy; Mx; My; Mxy]

% To do LPF just start the first FPF at some value
NM = [0;0;0;30.83;0;0];

laminate_strength_analysis;
% The warnings about the matrices being what and what
% are suppressed by a clc in the laminate_strength file
% in case you need to see them

lam_thickness = sum(tp_vec);

% Laminate Strength
% Fx = Nmax / laminate thickness