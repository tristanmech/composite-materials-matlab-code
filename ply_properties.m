% Carbon/Epoxy Unidirectional
% N/mm^2

E1 = 140e3;
E2 = 10e3;
G12 = 5e3;
v12 = 0.3;
v21 = (v12*E2)/E1;
tp = .125;
% theta = 0*ones(1,n_of_plies);
% % 
Xt = 1500;
Xc = 1200;
Yt = 50;
Yc = 250;
Sh = 70;
% ex_t = 0.0105;
% ex_c = 0.0085;
% ey_t = 0.005;
% ey_c = 0.025;
% es = 0.014;