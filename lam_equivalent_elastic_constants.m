function [Ex,Ey,vxy,vyx,mx,my,Gxy] = lam_equivalent_elastic_constants(tl,a,d,mode)
% Outputs the equivalent elastic constants for the laminate
% configuration.
% mode = 0 --> MEMBRANE MODE
% mode = 1 --> BENDING MODE
% tl = laminate thickness

if mode == 0
    Ex = 1/(tl*a(1,1));
    Ey = 1/(tl*a(2,2));
    Gxy = 1/(tl*a(3,3));
    vxy = -a(1,2)/a(1,1);
    vyx = -a(1,2)/a(2,2);
    mx = -a(1,3)/a(1,1);
    my = -a(2,3)/a(2,2);
elseif mode == 1
    Ex = 12/((tl^3)*d(1,1));
    Ey = 12/((tl^3)*d(2,2));
    Gxy = 12/((tl^3)*d(3,3));
    vxy = -d(1,2)/d(1,1);
    vyx = -d(1,2)/d(2,2);
    mx = -d(1,3)/d(1,1);
    my = -d(2,3)/d(2,2);
end
end