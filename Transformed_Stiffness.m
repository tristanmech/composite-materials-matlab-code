function y = Transformed_Stiffness(Q,mn_matrix)
% Get the transformed (x,y) stiffness matrix Trans_Q
%
% Syntax: y = Transformed_Stiffness(stiffness matrix,matrix containing the powers of m,n)
%

Trans_Q11 = mn_matrix(1,1)*Q(1,1) + mn_matrix(1,2)*Q(2,2) + ...
            mn_matrix(1,3)*Q(1,2) + mn_matrix(1,4)*Q(3,3);

Trans_Q22 = mn_matrix(2,1)*Q(1,1) + mn_matrix(2,2)*Q(2,2) + ...
            mn_matrix(2,3)*Q(1,2) + mn_matrix(2,4)*Q(3,3);

Trans_Q33 = mn_matrix(3,1)*Q(1,1) + mn_matrix(3,2)*Q(2,2) + ...
            mn_matrix(3,3)*Q(1,2) + mn_matrix(3,4)*Q(3,3);

Trans_Q12 = mn_matrix(4,1)*Q(1,1) + mn_matrix(4,2)*Q(2,2) + ...
            mn_matrix(4,3)*Q(1,2) + mn_matrix(4,4)*Q(3,3);

Trans_Q13 = mn_matrix(5,1)*Q(1,1) + mn_matrix(5,2)*Q(2,2) + ...
            mn_matrix(5,3)*Q(1,2) + mn_matrix(5,4)*Q(3,3);
            
Trans_Q23 = mn_matrix(6,1)*Q(1,1) + mn_matrix(6,2)*Q(2,2) + ...
            mn_matrix(6,3)*Q(1,2) + mn_matrix(6,4)*Q(3,3);

y = [
    Trans_Q11 Trans_Q12 Trans_Q13;
    Trans_Q12 Trans_Q22 Trans_Q23;
    Trans_Q13 Trans_Q23 Trans_Q33;
];
    
end