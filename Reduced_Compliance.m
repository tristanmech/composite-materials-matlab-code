function y = Reduced_Compliance(Q)
% Get the reduced compliance matrix by inverting the
% stiffness matrix
%
% Syntax: y = Reduced_Compliance(stiffness matrix)
%
% Long description
    

y = inv(Q);

end