% Chapter 5
% close all
% clear
% clc

% This vector will act as a material check
% e.g: 0 = steel, 1 = aluminium
% The first element is the bottom ply
% 0 - carbon/epoxy, 0 degrees
% 1 - carbon/epoxy, 45 degrees
% 2 - carbon/epoxy, -45 degrees
% 3 - carbon/epoxy, 90 degrees
% composition = [1;0;0;1];
% composition = [1;1;1;1;0;0;0;0;0;0;0;0;1;1;1;1];
% composition = [1;1;0;0;1;1;0;0;0;0;1;1;0;0;1;1];
% composition = [0;1;2;3;3;2;1;0];
% composition = [0;3]; 
% This will be supplied before the analysis
% If used alone, uncomment

n_of_plies = size(composition,1);
% Q matrix, again, FOR EACH PLY
% We are going to store each matrix here
% Q = zeros(3);
% Q_matrices = repmat(Q,n_of_plies,1);

% Alternate (better?) way - 3D array
Q = zeros(3,3,n_of_plies);

% Now calculate the reduced stiffness for each
% We are iterating over every ply, not every
% element of the matrices
% Remember we are counting starting from the bottom!

for i = 1:size(Q,3)
    if composition(i) == 0
        ply_properties;
    elseif composition(i) == 1
        ply_properties1;
    elseif composition(i) == 2
        ply_properties2;
    elseif composition(i) == 3
        ply_properties3
    elseif composition(i) == 4
        ply_properties4
    elseif composition(i) == 5
        ply_properties5
    end
    Q(:,:,i) = Reduced_Stiffness(E1,E2,G12,v12,v21);
end

S = zeros(3,3,n_of_plies);

for i = 1:size(Q,3)
    S(:,:,i) = inv(Q(:,:,i));
end

% Helper - Don't forget you can't use m or n now
% They are used for the transformations
% If we have varying ply angles this will need to change
% to iterate for each ply. Lets just do it now actually
% Angle for each ply 
% theta = [0; 0; 0; 0];

% Now get the transformed Q
trans_Q = zeros(size(Q));

for i = 1:size(Q,3)
    if composition(i) == 0
        ply_properties;
    elseif composition(i) == 1
        ply_properties1;
    elseif composition(i) == 2
        ply_properties2;
    elseif composition(i) == 3
        ply_properties3
    elseif composition(i) == 4
        ply_properties4
    elseif composition(i) == 5
        ply_properties5
    end
    alfred_iter;
    trans_Q(:,:,i) = Transformed_Stiffness(Q(:,:,i),mn_matrix);
end

trans_S = zeros(size(trans_Q));

for i = 1:size(trans_Q,3)
    trans_S(:,:,i) = inv(trans_Q(:,:,i));
end

% Coordinate Properties - FOR EACH PLY
% tp = ply thickness
% z_mp = distance of centroid from midplane
% We create a vector of tps to store the (sometimes)
% difference thicknesses
tp_vec = zeros(1,n_of_plies);

for i = 1:size(Q,3)
    if composition(i) == 0
        ply_properties;
    elseif composition(i) == 1
        ply_properties1;
    elseif composition(i) == 2
        ply_properties2;
    elseif composition(i) == 3
        ply_properties3
    elseif composition(i) == 4
        ply_properties4
    elseif composition(i) == 5
        ply_properties5
    end
    tp_vec(i) = tp;
end
tp_vec = tp_vec';
% z_mp = zeros(n_of_plies,1);

% for i = 1:size(Q,3)
%     if composition(i) == 1
%         ply_properties1;
%     elseif composition(i) == 0
%         ply_properties;
%     end
%     z_mp(i) = -
% end
z_mp = ((-(n_of_plies/2).*tp_vec)+ tp_vec./2):tp:...
        (((n_of_plies/2).*tp_vec) - tp_vec./2);
z_mp = z_mp';

bb = -tp_vec.*z_mp;
dd = tp_vec.*(z_mp.^2) + (tp_vec.^3)/12;
% Now for the laminate stiffness matrices
A = zeros(3,3,size(Q,3));
B = zeros(3,3,size(Q,3));
D = zeros(3,3,size(Q,3));

for i = 1:n_of_plies
    A(:,:,i) = (tp_vec(i))*trans_Q(:,:,i);
    B(:,:,i) = (bb(i))*trans_Q(:,:,i);
    D(:,:,i) = (dd(i))*trans_Q(:,:,i);
end

A = nansum(A,3);
B = nansum(B,3);
% This is needed in for matlab's inv() to work. Check the
% laminate_strength_analysis file for the reason
% B = B + eye(size(B))*1e-9;
% edit: maybe not if you do it another way
D = nansum(D,3);

% Compliances
a = zeros(size(Q));
b = zeros(size(Q));
d = zeros(size(Q));

for i = 1:n_of_plies
    a = inv(A);
    b = inv(B);
    d = inv(D);
end

% When we have a symmetric laminate the B matrix
% will be zero. This will throw a warning but ignore it
% (the matrix will be singular and matlab freaks out)

% Obtain equivalent elastic constants for each ply
% tl = laminate thickness
% _m = membrane constants
% _b = bending
tl = sum(tp_vec);

[Ex_m,Ey_m,vxy_m,vyx_m,mx_m,my_m,Gxy_m] = ...
    lam_equivalent_elastic_constants(tl,a,d,0);
[Ex_b,Ey_b,vxy_b,vyx_b,mx_b,my_b,Gxy_b] = ...
    lam_equivalent_elastic_constants(tl,a,d,1);

% Lets put them all somewhere to be able to see what is happening
membrane_equiv_const = [Ex_m; Ey_m; Gxy_m; vxy_m; vyx_m; mx_m; my_m];
bending_equiv_const = [Ex_b; Ey_b; Gxy_b; vxy_b; vyx_b; mx_b; my_b];