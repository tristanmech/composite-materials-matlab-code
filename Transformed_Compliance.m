function y = Transformed_Compliance(Trans_Q)
% Get the (x,y) compliance matrix from inverting the
% the transformed stiffness matrix
%
% Syntax: y = Transformed_Compliance(transformed stiffness matrix)
%

y = inv(Trans_Q);
    
end