m = cosd(theta(i)); % cos() --> radians cosd() --> degrees
n = sind(theta(i)); % see above

mn_values = [m m^2 m^3 m^4; n n^2 n^3 n^4];

mn_matrix = [
    mn_values(1,4) mn_values(2,4) 2*((mn_values(1,2))*mn_values(2,2)) 4*((mn_values(1,2))*mn_values(2,2));
    mn_values(2,4) mn_values(1,4) 2*((mn_values(1,2))*mn_values(2,2)) 4*((mn_values(1,2))*mn_values(2,2));
    (mn_values(1,2))*mn_values(2,2) (mn_values(1,2))*mn_values(2,2) -2*((mn_values(1,2))*mn_values(2,2)) ((mn_values(1,2)) - mn_values(2,2))^2;
    (mn_values(1,2))*mn_values(2,2) (mn_values(1,2))*mn_values(2,2) (mn_values(1,4) + mn_values(2,4)) -4*((mn_values(1,2))*mn_values(2,2));
    (mn_values(1,3)*mn_values(2,1)) -(mn_values(1,1)*mn_values(2,3)) ((mn_values(1,1)*mn_values(2,3)) - (mn_values(1,3)*mn_values(2,1))) 2*((mn_values(1,1)*mn_values(2,3)) - (mn_values(1,3)*mn_values(2,1)));
    (mn_values(1,1)*mn_values(2,3)) -(mn_values(1,3)*mn_values(2,1)) ((mn_values(1,3)*mn_values(2,1)) - (mn_values(1,1)*mn_values(2,3))) 2*((mn_values(1,3)*mn_values(2,1)) - (mn_values(1,1)*mn_values(2,3)));
];

% Matrix for doing axis transformations from reduced stiffness Q of a specially
% orthotropic ply to transformed Q of a ply with angle theta 
mn_matrix = [
    mn_values(1,4) mn_values(2,4) 2*((mn_values(1,2))*mn_values(2,2)) 4*((mn_values(1,2))*mn_values(2,2));
    mn_values(2,4) mn_values(1,4) 2*((mn_values(1,2))*mn_values(2,2)) 4*((mn_values(1,2))*mn_values(2,2));
    (mn_values(1,2))*mn_values(2,2) (mn_values(1,2))*mn_values(2,2) -2*((mn_values(1,2))*mn_values(2,2)) ((mn_values(1,2)) - mn_values(2,2))^2;
    (mn_values(1,2))*mn_values(2,2) (mn_values(1,2))*mn_values(2,2) (mn_values(1,4) + mn_values(2,4)) -4*((mn_values(1,2))*mn_values(2,2));
    (mn_values(1,3)*mn_values(2,1)) -(mn_values(1,1)*mn_values(2,3)) ((mn_values(1,1)*mn_values(2,3)) - (mn_values(1,3)*mn_values(2,1))) 2*((mn_values(1,1)*mn_values(2,3)) - (mn_values(1,3)*mn_values(2,1)));
    (mn_values(1,1)*mn_values(2,3)) -(mn_values(1,3)*mn_values(2,1)) ((mn_values(1,3)*mn_values(2,1)) - (mn_values(1,1)*mn_values(2,3))) 2*((mn_values(1,3)*mn_values(2,1)) - (mn_values(1,1)*mn_values(2,3)));
];

% Matrix for going from x-y to 1-2
% Stress f
mn_stress_ref_to_mat = [
    mn_values(1,2) mn_values(2,2) 2*mn_values(1,1)*mn_values(2,1);
    mn_values(2,2) mn_values(1,2) -2*mn_values(1,1)*mn_values(2,1);
    -mn_values(1,1)*mn_values(2,1) mn_values(1,1)*mn_values(2,1) ...
    mn_values(1,2) - mn_values(2,2);
];

% Strain e
mn_strain_ref_to_mat = [
    mn_values(1,2) mn_values(2,2) mn_values(1,1)*mn_values(2,1);
    mn_values(2,2) mn_values(1,2) -mn_values(1,1)*mn_values(2,1);
    -2*mn_values(1,1)*mn_values(2,1) 2*mn_values(1,1)*mn_values(2,1) ...
    mn_values(1,2) - mn_values(2,2);
];

% Matrix for going from 1-2 to x-y
% Stress
mn_stress_mat_to_ref = inv(mn_stress_ref_to_mat);
% Strain
mn_strain_mat_to_ref = inv(mn_strain_ref_to_mat);